@extends('layouts.app')
  
@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="pull-left">
            <h2>Add New Dayinc</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('dayincs.index') }}"> Back</a>
        </div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('dayincs.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Shiftday:</strong>
                <input type="date" name="shiftday" class="form-control" value="{{ date('Y-m-d') }}">
            </div>
	</div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Shift # of the day:</strong>
                <input type="number" name="shiftnumber" class="form-control" value="1">
            </div>
	</div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Sales person (you):</strong>
                <input type="text" name="salesperson" class="form-control" placeholder="Please type your name">
            </div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Cash at the beginning of the shift:</strong>
                <input type="number" name="cashstart" step="any" class="form-control" placeholder="90.00">
            </div>
        </div>
	<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Cash at the end of the shift:</strong>
                <input type="number" name="cashend" step="any" class="form-control" placeholder="144.00">
            </div>
        </div>
	<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Card sales:</strong>
                <input type="number" name="card" step="any" class="form-control" placeholder="133.11">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Comments:</strong>
                <textarea class="form-control" style="height:150px" name="comment" placeholder="Detail"></textarea>
            </div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Paid into bank account after shift:</strong>
                <input type="number" step="any" name="banked" class="form-control" placeholder="133.11">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
</div></div>
@endsection
