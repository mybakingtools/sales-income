@extends('layouts.app')
 
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="pull-left">
                <h2>Edit Dayinc</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('dayincs.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('dayincs.update',$dayinc->id) }}" method="POST">
        @csrf
        @method('PUT')
   
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form-group">
                    <strong>Shiftday:</strong>
                    <input type="text" name="shiftday" value="{{ $dayinc->shiftday }}" class="form-control" placeholder="Name">
                </div>
            </div>
	    </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form-group">
                    <strong>Shift # of the day:</strong>
                    <input type="number" name="shiftnumber" value="{{ $dayinc->shiftnumber }}" class="form-control" value="1">
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="form-group">
                <strong>Sales person (you):</strong>
                <input type="text" name="salesperson" value="{{ $dayinc->salesperson }}" class="form-control" placeholder="Please type your name">
            </div>
        </div>
        </div>
        <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="form-group">
                <strong>Cash at the beginning of the shift:</strong>
                <input type="number" name="cashstart" value="{{ $dayinc->cashstart }}" step="any" class="form-control" placeholder="">
            </div>
        </div>
        </div>
        <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="form-group">
                <strong>Cash at the end of the shift:</strong>
                <input type="number" name="cashend" value="{{ $dayinc->cashend }}" step="any" class="form-control" placeholder="">
            </div>
        </div>
        </div>
        <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="form-group">
                <strong>Card sales:</strong>
                <input type="number" name="card" value="{{ $dayinc->card }}" step="any" class="form-control" placeholder="133.11">
            </div>
        </div>
        </div>
        <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="form-group">
                <strong>Comments:</strong>
                <textarea class="form-control" style="height:150px" name="comment" placeholder="">{{ $dayinc->comment }}</textarea>
            </div>
        </div>
        </div>
        <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="form-group">
                <strong>Paid into bank account after shift:</strong>
                <input type="number" name="banked" value="{{ $dayinc->banked }}" step="any" class="form-control" placeholder="">
            </div>
        </div>
        </div>
        <div class="row justify-content-center">
        <div class="col-md-8">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        </div>
    </form>
</div></div>
@endsection
