@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="pull-left">
                <h2>Show Dayinc</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('dayincs.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="form-group">
                <strong>Shiftday:</strong>
                {{ $dayinc->shiftday }}
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <strong>Shift #:</strong>
                {{ $dayinc->shiftnumber }}
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <strong>Sales Person:</strong>
                {{ $dayinc->salesperson }}
            </div>
        </div>
        <div class="col-md-8">
	    <div class="form-group">
		<table>
		<tr><td>Cash before shift</td><td>{{ $dayinc->cashstart }}</td></tr>
		<tr><td>Cash after shift</td><td>{{ $dayinc->cashend }}</td></tr>
		<tr><td>Cash total</td><td>{{ $dayinc->cashtotal }}</td></tr>
		<tr><td>Card total</td><td>{{ $dayinc->card }}</td></tr>
		<tr><td>Total incl. gst</td><td>{{ $dayinc->totalincgst }}</td></tr>
		<tr><td>GST</td><td>{{ $dayinc->gst }}</td></tr>
		<tr><td>Total excl. gst</td><td>{{ $dayinc->totalexclgst }}</td></tr>
		</table>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <strong>Paid into bank account after day:</strong>
                {{ $dayinc->banked }}
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <strong>Comment:</strong>
                {{ $dayinc->comment }}
            </div>
        </div>
    </div>
</div></div>
@endsection
