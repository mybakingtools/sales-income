@extends('layouts.app')
 
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="pull-left">
                <h2>Test App</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('dayincs.create') }}"> Create New Dayinc</a>
            </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
  </div></div>
  <div class="row justify-content-center">
        <div class="col-md-8">
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Shiftday</th>
	    <th>Shift#</th>
	    <th>Cash</th>
	    <th>Card</th>
	    <th>Total</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($dayincs as $dayinc)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $dayinc->shiftday }}</td>
	    <td>{{ $dayinc->shiftnumber }}</td>
	    <td>{{ $dayinc->cashtotal }}</td>
	    <td>{{ $dayinc->card }}</td>
	    <td>{{ $dayinc->totalexclgst }}</td>
            <td>
                <form action="{{ route('dayincs.destroy',$dayinc->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('dayincs.show',$dayinc->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('dayincs.edit',$dayinc->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $dayincs->links() !!}
</div>      
    </div>
@endsection
