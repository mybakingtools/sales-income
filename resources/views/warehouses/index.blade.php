@extends('layouts.app')
 
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="pull-left">
                <h2>Warehouses</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('warehouses.create') }}"> Create New Warehouse</a>
            </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
  </div></div>
  <div class="row justify-content-center">
        <div class="col-md-8">
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Code</th>
	        <th>Name</th>
	        <th width="280px">Action</th>
        </tr>
        @foreach ($warehouses as $warehouse)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $warehouse->code }}</td>
            <td>{{ $warehouse->name }}</td>
            <td>
                <form action="{{ route('warehouses.destroy',$warehouse->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('warehouses.show',$warehouse->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('warehouses.edit',$warehouse->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $warehouses->links() !!}
</div>      
    </div>
@endsection
