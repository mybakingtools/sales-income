@extends('layouts.app')
  
@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="pull-left">
            <h2>Add New Warehouse</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('warehouses.index') }}"> Back</a>
        </div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('warehouses.store') }}" method="POST">
    @csrf
  
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Warehouse Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Please type the name of the warehouse">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Warehouse Code:</strong>
                <input type="text" name="code" class="form-control" placeholder="Please type the name of the warehouse">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
</div></div>
@endsection
