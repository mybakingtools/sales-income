@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="pull-left">
                <h2>Show Dayinc</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('warehouses.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="form-group">
                <strong>Code:</strong>
                {{ $warehouse->code }}
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $warehouse->name }}
            </div>
        </div>
    </div>

@endsection
