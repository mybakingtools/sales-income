@extends('layouts.app')
 
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="pull-left">
                <h2>Edit Dayinc</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('warehouses.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('warehouses.update',$warehouse->id) }}" method="POST">
        @csrf
        @method('PUT')
   
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form-group">
                    <strong>Code:</strong>
                    <input type="text" name="code" value="{{ $warehouse->code }}" class="form-control" placeholder="Warehouse Code">
                </div>
            </div>
	    </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form-group">
                    <strong>Code:</strong>
                    <input type="text" name="name" value="{{ $warehouse->name }}" class="form-control" placeholder="Warehouse Name">
                </div>
            </div>
	    </div>

        <div class="row justify-content-center">
        <div class="col-md-8">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        </div>
    </form>
</div></div>
@endsection
