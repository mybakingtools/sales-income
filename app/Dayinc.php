<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dayinc extends Model
{
     protected $fillable = [
	     'shiftday',
	     'shiftnumber',
	     'salesperson',
	     'cashstart',
	     'cashend',
	     'cashtotal',
	     'gst',
	     'totalincgst',
	     'totalexclgst',
	     'card',
	     'banked',
	     'comment',
    ];
}
