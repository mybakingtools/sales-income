<?php

namespace App\Http\Controllers;

use App\Dayinc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class DayincController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {         
       $dayincs = Dayinc::latest()->paginate(7);
        return view('dayincs.index',compact('dayincs'))
            ->with('i', (request()->input('page', 1) - 1) * 5); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                return view('dayincs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'shiftday' => 'required',
            'shiftnumber' => 'required',
        ]);
  	$this->updateTotals($request);
 print_r($request->all()); 
        Dayinc::create($request->all());
   
        return redirect()->route('dayincs.index')
                        ->with('success','Dayinc created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dayinc  $dayinc
     * @return \Illuminate\Http\Response
     */
    public function show(Dayinc $dayinc)
    {
               return view('dayincs.show',compact('dayinc'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dayinc  $dayinc
     * @return \Illuminate\Http\Response
     */
    public function edit(Dayinc $dayinc)
    {
               return view('dayincs.edit',compact('dayinc'));
 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dayinc  $dayinc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dayinc $dayinc)
    {
       $request->validate([
            'shiftday' => 'required',
            'shiftnumber' => 'required',
    ]);
  	$this->updateTotals($request);
        $dayinc->update($request->all());
  
        return redirect()->route('dayincs.index')
                        ->with('success','Dayinc updated successfully'); 
    }

    private function updateTotals(&$request) 
    {
	    $cashstart = $request->input('cashstart');
	    $cashend = $request->input('cashend');
	    $card = $request->input('card');
	    echo 'start: ' . $cashstart;
	    $cash = null;
	    if (!empty($cashstart) && !empty($cashend) && $cashend*1 > 0) {
		    $cash = $cashend - $cashstart;
	    }

	    if ($cash !== null) {
		    $request->request->add(['cashtotal' => $cash]);
	    }
	    if ($cash !== null && $card !== null) {
		    $totalinclgst = round($cash + $card, 2);
		    $totalexclgst = round($totalinclgst/1.15, 2);
		    $gst = round($totalinclgst-$totalexclgst);
	    	    $request->request->add(['totalincgst' => $totalinclgst]);
		    $request->request->add(['gst' => $gst]);
		    $request->request->add(['totalexclgst' => $totalexclgst]);
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dayinc  $dayinc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dayinc $dayinc)
    {
	  $dayinc->delete();
  
        return redirect()->route('dayincs.index')
                        ->with('success','Dayinc deleted successfully');
    }
}
