<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Restricted to users with the show-dayinc permission.
 */
Route::resource('dayincs', 'DayincController')->middleware('can:show-dayinc');

/**
 * For all logged in users
 */
Route::resource('warehouses', 'WarehouseController')->middleware('auth');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes(['register' => false]);
