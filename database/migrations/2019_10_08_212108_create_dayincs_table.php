<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDayincsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dayincs', function (Blueprint $table) {
		$table->bigIncrements('id');
                $table->string('comment')->nullable();
                $table->date('shiftday');
		$table->integer('shiftnumber');
		$table->string('salesperson', 100)->nullable();
                $table->decimal('cashstart', 8, 2)->nullable();
		$table->decimal('cashend', 8, 2)->nullable();
		$table->decimal('cashtotal', 8, 2)->nullable();
                $table->decimal('card', 8, 2)->nullable();
                $table->decimal('totalincgst', 8, 2)->nullable();
                $table->decimal('gst', 8, 2)->nullable();
                $table->decimal('totalexclgst', 8, 2)->nullable();
                $table->decimal('banked', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dayincs');
    }
}
